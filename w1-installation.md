# Hi!
Welcome to the Introduction to Keycloak workshop!

## Keycloak and/or Red Hat SSO

* Download Keycloak or install Red Hat SSO: https://www.keycloak.org/downloads
* unzip it
* run it with `bin/standalone.sh` or `bin/standalone.bat` (you will of course need java for this)
* navigate to: http://localhost:8080/auth/
* create your admin account using the form
* see if it works by navigating to navigate to http://localhost:8080/auth/admin/

## Maven
Maven is written in Java. So, to run Maven, we need a system that has Java installed and configured properly. We can download an OS-compatible Java JDK from Oracle's download site, for example.  It's recommended to install it to a pathname without spaces.

Once Java is installed, we need to ensure that the commands from the Java JDK are in our PATH environment variable. Running, for example:
`java -version`
must display the right version number.

### Windows

To install Maven on windows, we head over to the [Apache Maven](https://maven.apache.org/download.cgi) site to download the latest version and select the Maven zip file, for example, apache-maven-3.3.9-bin.zip.

Then we unzip it to the folder where we want Maven to live.

We add both _M2_HOME_ and _MAVEN_HOME_ variables to the Windows environment using system properties and point it to our Maven folder.

Then we update the PATH variable by appending the Maven bin folder — `%M2_HOME%\bin` — so that we can run the Maven command everywhere.

To verify it, we run:
`mvn -version`
in the command prompt. It should display the Maven version, the java version, and the operating system information. That's it. We've set up Maven on our Windows system.

### Linux
The Maven package always comes with the latest Apache Maven.

We run the command sudo apt-get install maven to install the latest Apache Maven.

`$ sudo apt-get install maven`

This will take a few minutes to download. Once downloaded, we can run the `mvn -version` to verify our installation.

## Git

### Windows

To install git on windows we need to go to the website https://git-scm.com/ and install git from the download link. To check if git is working on your machine use the command: `git --version`
### Linux

We run the command sudo apt-get install maven to install the latest Apache Maven.

`$ sudo apt-get install git`

This will take a few minutes to download. Once downloaded, we can run the `git --version` to verify our installation.

## Creating your first Realm

Hover over the Master realm (Top Left corner) and click on "Add realm".
![image](images/add-realm-menu.png)

Fill in the name: `workshop` and click create.
![image](images/create-realm.png)

You are now ready to secure your first application.
