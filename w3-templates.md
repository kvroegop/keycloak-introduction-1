# Templates

## Adding customization

Let's take the example of our Account Management page and see how to change its appearance. To be precise, we'll be changing the logo appearing on the page.

! warning !
If you are using keycloak 15.x.x or higher. Change the theme to the older keycloak theme. this version of keycloak has an updated theme for account with this task is not based on. Go to Realm Settings in de side menu and click on Themes in the top menu. change the following values:

* Account Theme: keycloak

Just before we'll do all the changes, below is the original template, available at http://localhost:8080/auth/realms/workshop/account

![image](images/AccountUser1Before-1536x528-1-768x264.png)

Let's try to change the logo to our own. For that, we need to add a new folder, account inside the `themes` directory. We'll rather copy it from the `themes/keycloak` directory so that we have all the required elements.
 
Now, it's just a matter of adding our new logo file, say first8.png to `resources/img` in our custom directory.
A First8 logo (first8.png) is located right here in the same folder where your instructions are.

Next, modify `account/resources/css/account.css`:

    .navbar-title {
       background-image: url('../img/first8.png');
       height: 25px;
       background-repeat: no-repeat;
       width: 123px;
       margin: 3px 10px 5px;
       text-indent: -99999px;
     }

    .container {
      height: 100%;
      background-color: #fff;
    }

And here's how the page looks now:

![image](images/account-page.png)

**Importantly, during the development phase, we'll like to see the effect of our changes immediately, without a server restart.** To enable that, we need to make a few changes to Keycloak's standalone.xml in the standalone/configuration folder:

    <theme>
        <staticMaxAge>-1</staticMaxAge>
        <cacheThemes>false</cacheThemes>
        <cacheTemplates>false</cacheTemplates>
        ...
    </theme>

Similar to how we customized the account theme here, to change the look and feel of the other theme types, we need to add new folders called admin, email, or login, and follow the same process.
